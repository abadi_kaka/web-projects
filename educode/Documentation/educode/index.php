<!DOCTYPE html>
<html>
  <head>    
    <meta charset="UTF-8">
	<title>SINAWU</title>
    <meta name="description" content="GRAPE App landing Page by Codepassenger">
    <meta name="author" content="Jems, Okke, Abadi">
	
	<!-- Mobile Specific Meta -->	
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Font awesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!-- Animate.css -->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!-- Owl-carousel style -->
    <link rel="stylesheet" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" href="assets/css/owl.theme.css">

    <!-- magnific style -->
    <link rel="stylesheet" href="assets/css/magnific-popup.css">

    <!-- Custom stylesheet-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css" media="screen">

    <!-- Added google font -->
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <!--Fav and touch icons-->
    <link rel="shortcut icon" href="assets/images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/icons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/icons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/icons/apple-touch-icon-114x114.png">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {packages: ['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        // Define the chart to be drawn.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Element');
        data.addColumn('number', 'Percentage');
        data.addRows([
          ['Nitrogen', 0.78],
          ['Oxygen', 0.21],
          ['Other', 0.01]
        ]);

      // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('myPieChart'));
        chart.draw(data, null);

    }
    </script>
  </head>
  <body>
	<!-- Preloader -->
      <div id="faceoff">
        <div id="preloader"></div>
        <div class="preloader-section"></div>
      </div>
    <!-- Preloader end -->
  
    <!-- header start -->
    <header id="home" class="navbar-fixed-top">
      <nav class="navbar navbar-default" role="navigation">
        <div class="container">
          <div class="row">
            <div class="col-sm-3">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <h1><a class="nav-brand" href=""> <img src="assets/images/logo.png" alt="gigninga"></a></h1>
              </div>
            </div>
            <div class="col-sm-7">
              <!-- Collect the nav toggling -->
              <div class="collapse navbar-collapse navbar-example" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li><a href="#intro">Intro</a></li>
                  <li><a href="#feature">Feature</a></li>
                  <li><a href="#description">Description</a></li>
                  <li><a href="#screenshot">Screenshots</a></li>
                  <li><a href="#download">Download</a></li>
                  <li><a href="#contact">Contact</a></li>
                </ul>             
              </div><!-- /.navbar-collapse -->
            </div>
            <div class="col-sm-2 mob-right">
              <ul class="app pull-right">
                <li><a href="" ><i class="fa fa-apple"></i></a></li>
                <li><a href="" ><i class="fa fa-android"></i></a></li>
                <li><a href="" ><i class="fa fa-windows"></i></a></li>
              </ul>
            </div>
          </div>
        </div><!-- /.container -->
      </nav>
    </header>
    <!-- header end -->

    <!-- banner start -->
    <section id="banner" class="banner">
      <div class="trans-bg" style="background:rgba(76,175,80,0.7) none repeat scroll 0% 0%">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">              
              <h2><a href="" class="lg-logo default-color">SINAWU</a></h2> <!--/ .lg-logo -->
               <div class="flex_text text-slider">
                  <ul class="slides">
                      <li>Kemudahan belajar dalam aplikasi SINAWU</li>
                      <li>Jadilah Volunter yang berdedikasi dalam pendidikan</li>
                      <li>Masa depan pendidikan Indonesia ada di tangan anda</li>
                  </ul>
                </div>
                <!--/.text-slider end-->                
                <ul class="app">
                  <li><a href=""><i class="fa fa-apple"></i></a></li>
                  <li><a href=""><i class="fa fa-android"></i></a></li>
                  <li><a href=""><i class="fa fa-windows"></i></a></li>
                </ul>  
                <div class="download-block text-center">
                   <a href="" class="btn-download">Download</a><!-- <a href="" class="btn-download">Take a tour</a> -->
                 </div>             
            </div>
          </div>
        </div>
      </div> <!-- /.trans-bg -->                 
    </section>
    <!-- banner end -->

    <!-- intro start -->
    <section id="intro" class="intro white">

    
<!--        <div id="myPieChart"></div> -->

      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <span class="sub-head wow fadeInLeft">UKDW-3</span>
            <div class="title wow fadeInRight">
              <h2 style="color:rgb(76,175,80)">SINAWU</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="app-block wow bounceInUp" data-wow-delay=".1s">
              <i class="fa fa-bolt"></i>
              <h3>Volunter Pengajar</h3>
              <p>Banyaknya volunter pengajar di SINAWU memudahkan orangtua dan atau wali untuk mencarikan pengajar yang tepat dan sesuai dengan keinginan mereka. Tidak hanya tenaga pengajar pendidikan formal namun segala macam tenaga  kerja pendidikan informal pun banyak ditawarkan di SINAWU</p>
            </div>            
          </div>
          <div class="col-sm-4">
            <div class="app-block wow bounceInUp" data-wow-delay=".2s">
              <i class="fa fa-life-ring"></i>
              <h3>Fleksibilitas</h3>
              <p>Kapanpun dimana pun dan memudahkan murid untuk belajar dengan menyenangkan dan interaktif</p>
            </div>            
          </div>
          <div class="col-sm-4">
            <div class="app-block wow bounceInUp" data-wow-delay=".3s">
              <i class="fa fa-paint-brush"></i>
              <h3>Desain Unik</h3>
              <p>Desain aplikasi yang memudahkan user dalam menggunakan aplikasi untuk memilih dan mendapatkan report</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="app-block wow bounceInUp" data-wow-delay=".4s">
              <i class="fa fa-angellist"></i>
              <h3>Selektif</h3>
              <p>Kualitas tenaga pengajar yang mumpuni dan dipilih dengan kualifikasi tertentu serta informasi akurat antar volunter tenaga pengajar </p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="app-block wow bounceInUp" data-wow-delay=".5s">
              <i class="fa fa-thumbs-o-up"></i>
              <h3>Timbal Balik dan Respon</h3>
              <p>Mendapatkan laporan dan perkembangan anak asuh setiap volunter kepada orangtua untuk melihat progress pembelajaran murid</p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="app-block wow bounceInUp" data-wow-delay=".6s">
              <i class="fa fa-puzzle-piece"></i>
              <h3>Pengalaman Volunter</h3>
              <p>Pengalaman volunter untuk membawa kemajuan di bidang pendidikan indonesia sangat membantu kehidupan sosial pendidikan di tanah air. Segudang pengalaman akan mengubah hidup anda</p>
            </div>            
          </div>
        </div>
      </div>
    </section>
    <!-- intro end -->

    <!-- feature start -->
    <section id="feature" class="feature">
      <div class="trans-bg">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center">
              <h2 class="wow swing" data-wow-delay=".1s">crazy feature</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">
              <ul class="feature-list-left">
                <li>
                  <div class="feature-detail wow fadeInLeft" data-wow-delay=".4s">
                    <h4>Volunter Terkualifikasi</h4>
                    <p>Volunter yang bekerja di SINAWU terpilih berdasarkan syarat dan kualifikasi yang jelas</p>
                  </div>                          
                  <div class="feature-icon wow fadeInLeft" data-wow-delay=".3s">
                    <i class="fa fa-exchange"></i>
                  </div>
                </li>
                <li>
                  <div class="feature-detail wow fadeInLeft" data-wow-delay=".4s">
                    <h4>Pendidikan Formal and Informal</h4>
                    <p>Tidak hanya berfokus pada pendidikan formal namun bisa juga untuk pendidikan informal seperti pelajaran musik</p>
                  </div>
                  <div class="feature-icon wow fadeInLeft" data-wow-delay=".3s">
                    <i class="fa fa-star-half-o"></i>
                  </div>
                </li>
                <li>
                  <div class="feature-detail wow fadeInLeft" data-wow-delay=".4s">
                    <h4>Aplikasi Responsif</h4>
                    <p>Aplikasi memudahkan user dalam penggunaan sehingga user merasa nyaman dalam menggunakan aplikasi</p>
                  </div>
                  <div class="feature-icon wow fadeInLeft" data-wow-delay=".3s">
                    <i class="fa fa-arrows-alt"></i>
                  </div>                  
                </li>
                <li>
                  <div class="feature-detail wow fadeInLeft" data-wow-delay=".4s">
                    <h4>Grafik Progres</h4>
                    <p>Progres murid tentang perkembangan murid akan disajikan untuk orangtua untuk pemantauan</p>
                  </div>
                  <div class="feature-icon wow fadeInLeft" data-wow-delay=".3s">
                    <i class="fa fa-cog"></i>
                  </div>
                </li>
              </ul>
            </div>
            <div class="col-sm-4 text-center">
              <img class="wow bounceInUp" src="assets/images/iphone_full.png" alt="" title="">
            </div>
            <div class="col-sm-4">
              <ul class="feature-list-right">
                <li>
                  <div class="feature-icon wow fadeInRight" data-wow-delay=".3s">
                    <i class="fa fa-arrows-alt"></i>
                  </div>
                  <div class="feature-detail wow fadeInRight" data-wow-delay=".4s">
                    <h4>Detail Lokasi</h4>
                    <p>Pemberian lokasi agar mengetahui letak dan posisi siswa maupun volunter</p>
                  </div>
                </li>
                <li>
                  <div class="feature-icon wow fadeInRight" data-wow-delay=".3s">
                    <i class="fa fa-cog"></i>
                  </div>
                  <div class="feature-detail wow fadeInRight" data-wow-delay=".4s">
                    <h4>Mudah Dipakai</h4>
                    <p>Sangat mudah digunakan baik sebagai orangtua maupun volunter dengan tampilan interaktif</p>
                  </div>
                </li>
                <li>
                  <div class="feature-icon wow fadeInRight" data-wow-delay=".3s">
                    <i class="fa fa-mobile"></i>
                  </div>
                  <div class="feature-detail wow fadeInRight" data-wow-delay=".4s">
                    <h4>Multiple Platform</h4>
                    <p>Dapat ditemukan di IOS, Windows Phone, Android maupun website</p>
                  </div>
                </li>
                <li>
                  <div class="feature-icon wow fadeInRight" data-wow-delay=".3s">
                    <i class="fa fa-undo"></i>
                  </div>
                  <div class="feature-detail wow fadeInRight" data-wow-delay=".4s">
                    <h4>Jadwal Mengajar</h4>
                    <p>Adanya jadwal mengajar yang jelas dan dapat dipilih sesuai keinginan</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div><!-- /.trans-bg -->
    </section>
    <!-- feature end -->

    <!-- description start -->
    <section id="description" class="description white">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <span class="sub-head wow fadeInLeft">pikir lagi</span>
            <div class="title wow fadeInRight">
              <h2>kenapa memilih SINAWU</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 wow pulse" data-wow-delay=".5s">
            <img class="img-responsive" src="assets/images/iphone_half.png" alt="Why to choose us">
          </div>
          <div class="col-sm-6">
            <ul class="description-list">
              <li class="wow fadeInRight"><span class="des-icon"><i class="fa fa-bomb"></i></span>Membantu pendidikan di Indonesia dengan teknologi</li>
              <li class="wow fadeInRight"><span class="des-icon"><i class="fa fa-paw"></i></span>Menyalurkan nilai moral dan hidup bagi volunter</li>
              <li class="wow fadeInRight"><span class="des-icon"><i class="fa fa-birthday-cake"></i></span>Memberikan pengalaman bekerja untuk sesama yang membutuhkan </li>
              <li class="wow fadeInRight"><span class="des-icon"><i class="fa fa-fire-extinguisher"></i></span>Meringankan biaya orangtua dan wali dalam memberikan ilmu </li>
              <li class="wow fadeInRight"><span class="des-icon"><i class="fa fa-magic"></i></span>Menumbuhkan jiwa sosial dan memberikan pelayanan kepada masyarakat</li>
              <li class="wow fadeInRight"><span class="des-icon"><i class="fa fa-rebel"></i></span>Laporan dan informasi yang terstruktur bagi orangtua</li>
            </ul>
            <!-- /.description-list -->
          </div>
        </div>
      </div>
    </section>
    <!-- description end-->

    <!-- video start -->
    <section id="video" class="video">
      <div id="trans-bg" class="trans-bg">
      	<div class="video-container"><div id="player"></div><div class="video-mask"></div></div>
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center">
              <div class="video-mask">
                <h2 class="wow bounceInDown">bagaimana alur kerjanya</h2>
					<a href="https://vimeo.com/45830194" class="play wow rollIn animated"></a>
                <span class="wow bounceInUp">lihat video</span>
              </div>            
            </div>
          </div>
        </div> <!-- /.trans-bg -->
      </div>
    </section>
    <!-- video end -->

    <!-- screenshot start -->
    <section id="screenshot" class="screenshot white">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <span class="sub-head wow fadeInLeft">sneek peek</span>            
            <div class="title wow fadeInRight">
              <h2>screenshots</h2>
            </div>
            <div id="owl-demo" class="owl-carousel">
                <div class="item screenshot-block">
                  <img src="./assets/images/screenshot.jpg" alt="GRAPE">
                  <div class="caption">
                    <a class="test-popup-link" href="./assets/images/screenshot.jpg"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
                <div class="item screenshot-block">
                  <img src="./assets/images/screenshot.jpg" alt="GRAPE">
                  <div class="caption">
                    <a class="test-popup-link" href="./assets/images/screenshot.jpg"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
                <div class="item screenshot-block">
                  <img src="./assets/images/screenshot.jpg" alt="GRAPE">
                  <div class="caption">
                    <a class="test-popup-link" href="./assets/images/screenshot.jpg"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
                <div class="item screenshot-block">
                  <img src="./assets/images/screenshot.jpg" alt="GRAPE">
                  <div class="caption">
                    <a class="test-popup-link" href="./assets/images/screenshot.jpg"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
                <div class="item screenshot-block">
                  <img src="./assets/images/screenshot.jpg" alt="GRAPE">
                  <div class="caption">
                    <a class="test-popup-link" href="./assets/images/screenshot.jpg"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
                <div class="item screenshot-block">
                  <img src="./assets/images/screenshot.jpg" alt="GRAPE">
                  <div class="caption">
                    <a class="test-popup-link" href="./assets/images/screenshot.jpg"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- screenshot end -->

    <!-- review start -->
    <section id="review" class="review">
      <div class="trans-bg"> 
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center">
              <h2 class="wow swing">reviews</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 text-center">
              <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="active item">
                    <span class="review-img">
                       <img src="./assets/images/dwiki.jpg" style="border-radius:50%;width:151px;height:151px;" alt="image">
                     </span>
                    <p>Menurut saya dengan SINAWU saya belajar untuk mengajari anak-anak yang benar membutuhkan materi tambahan maupun baru yang tidak didapat di sekolah. Selain itu aplikasi ini sangat membantu Indonesia dalam dunia pendidikan Indonesia</p>
                    <span class="reviewer-name">Dwicky Pramudita</span>
                    <span class="review-comp">UKDW, Pengajar Bahasa Inggris.</span>
                  </div>
                  <div class="item">
                    <span class="review-img">
                       <img src="./assets/images/johan.jpg" style="border-radius:50%;width:151px;height:151px;" alt="image">
                     </span>
                    <p>SINAWU membantu saya memahami materi dengan lebih baik dan juga membantu saya memahami materi yang kurang dimengerti di sekolah. Selain itu pengajarnya juga berkualitas.</p>
                    <span class="reviewer-name">Johan Sutanto</span>
                    <span class="review-comp">SMAN 1 Jakarta, Siswa Kelas XI-IPA.</span>
                  </div>
                  <div class="item">
                    <span class="review-img">
                       <img src="./assets/images/dina.jpg" style="border-radius:50%;width:151px;height:151px;" alt="image">
                     </span>
                    <p>SINAWU sungguh membantu anak saya dalam memperoleh nilai bagus di sekolah. Selain itu dengan SINAWU saya dapat mengetahui perkembangan progress anak saya dengan terarah.</p>
                    <span class="reviewer-name">Maria Dina</span>
                    <span class="review-comp">Orangtua Siswa.</span>
                  </div>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel1" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                <a class="carousel-control right" href="#myCarousel1" data-slide="next"><i class="fa fa-angle-right"></i></a>              
              </div>
            </div>
          </div>
        </div>
      </div> <!-- /.trans-bg -->  
    </section>
    <!-- review end -->

    <!-- price start -->
    <section id="price" class="pricingtable-section white">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <span class="sub-head wow fadeInLeft">Kondisi Pendidikan Indonesia</span>            
            <div class="title wow fadeInRight">
              <h2>Data & Fakta</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div id="chart_div" style="width: 900px; height: 500px"></div>
        </div>
        <!-- <div class="row">
          <div class="col-sm-4 wow bounceInUp" data-wow-duration="1s">
            <div class="price-block">
              <div class="price-header">
                <h3>free plan</h3>
                <i class="fa fa-heart"></i>
              </div>
              <div class="price-plan">
                <ul>
                  <li>
                    <span class="price">$0.00</span>
                    <span class="month">/per month</span>
                  </li>
                  <li>Unlimited Space</li>
                  <li>Unlimited Database</li>
                  <li>Easy to Istall</li>
                  <li>Unlimited Subdomains</li>
                  <li>Dedicated Serer</li>
                  <li> <a href="" class="btn">sign up</a></li>
                </ul>
              </div>
            </div> -->
            <!-- /.price-block -->
          <!-- <div class="col-sm-4 wow bounceInUp" data-wow-duration="2s">
            <div class="price-block active">
              <div class="price-header">
                <h3>basic plan</h3>
                <i class="fa fa-cogs"></i>
              </div>
              <div class="price-plan">
                <ul>
                  <li>
                    <span class="price">$9.00</span>
                    <span class="month">/per month</span>
                  </li>
                  <li>Unlimited Space</li>
                  <li>Unlimited Database</li>
                  <li>Easy to Istall</li>
                  <li>Unlimited Subdomains</li>
                  <li>Dedicated Serer</li>
                  <li> <a href="" class="btn">sign up</a></li>
                </ul>
              </div>
            </div> -->
            <!-- /.price-block -->
          <!-- </div>
          <div class="col-sm-4 wow bounceInUp" data-wow-duration="3s">
           -->  <!-- <div class="price-block">
              <div class="price-header">
                <h3>devloper plan</h3>
                <i class="fa fa-suitcase"></i>
              </div>
              <div class="price-plan">
                <ul>
                  <li>
                    <span class="price">$29.00</span>
                    <span class="month">/per month</span>
                  </li>
                  <li>Unlimited Space</li>
                  <li>Unlimited Database</li>
                  <li>Easy to Istall</li>
                  <li>Unlimited Subdomains</li>
                  <li>Dedicated Serer</li>
                  <li> <a href="" class="btn">sign up</a></li>
                </ul>
              </div>
            </div> -->
            <!-- /.price-block -->
          
      </div>
    </section>
    <!-- price end -->

    <!-- download start -->
    <section id="download" class="downlaod">
      <div class="trans-bg">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center">
              <h2 class="wow swing">download</h2>
              <p>Silahkan unduh aplikasi SINAWU di platform anda</p>
              <span class="wow fadeInLeft" style="display:inline-block"><a href="" class="btn"><i class="fa fa-apple"></i> app store</a></span>
              <span class="wow fadeInLeft" style="display:inline-block"><a href="" class="btn"><i class="fa fa-android"></i> play store</a></span>
              <span class="wow fadeInLeft" style="display:inline-block"><a href="" class="btn"><i class="fa fa-windows"></i> windows</a></span>
            </div>
          </div>
        </div>
      </div> <!-- /.trans-bg -->  
    </section>
    <!-- download end -->

    <!-- support start -->
    <section id="support" class="support white">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <span class="sub-head wow fadeInLeft">stay close</span>
            <div class="title wow fadeInRight">
              <h2>help & support</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4 wow bounceInUp" data-wow-duration="1s">
            <div class="support-block">
              <span class="support-icon"><i class="fa fa-bitbucket"></i></span>
              <h3>Volunter Support</h3>
              <p>Berikan dukungan anda dengan memberikan feedback berupa kritik dan saran terhadap para volunter kami</p>
            </div>
          </div>
          <div class="col-sm-4 wow bounceInUp" data-wow-duration="2s">
            <div class="support-block active">
              <span class="support-icon"><i class="fa fa-bullhorn"></i></span>
              <h3>Product Support</h3>
              <p>Berikan dukungan anda dengan memberikan feedback berupa kritik dan saran terhadap produk kami</p>
            </div>
          </div>
          <div class="col-sm-4 wow bounceInUp" data-wow-duration="3s">
            <div class="support-block">
              <span class="support-icon"><i class="fa fa-stethoscope"></i></span>
              <h3>External Support</h3>
              <p>Berikan dukungan anda dengan memberikan feedback berupa hal lain maupun bisa berupa kerjasama partnership, sponsorship maupun donatur</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- support end --> 

    <!-- subscription start -->
    <section id="subscription" class="subscription"> 
      <div class="trans-bg">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
            <h2 class="wow swing">daftar kiriman berita harian</h2>
            <form class="newsletter-signup">
              <p class="newsletter-success"></p>
              <p class="newsletter-error"></p>
              <div class="input-group">
                <input type="email" name="email" placeholder="Enter Your Email Address" class="form-control wow fadeInLeft">
                <span class="input-group-btn wow fadeInRight">
                <input type="submit" value="subscribe" class="btn btn-sub">
                </span>
              </div><!-- /.input-group -->
            </form>
            </div>
          </div>
        </div>
      </div> <!-- /.trans-bg --> 
    </section>
    <!-- subscription end -->

    <!-- contact start -->
    <section id="contact" class="contact white tab-content">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs text-center" role="tablist">
              <li class="wow rollIn animated" data-wow-duration="1s"><a href="#contact-info" role="tab" data-toggle="tab"><i class="fa fa-paper-plane"></i>
                </a></li>
              <li class="active wow rollIn animated" data-wow-duration="2s"><a href="#contact-form" role="tab" data-toggle="tab"><i class="fa fa-envelope"></i></a></li>
              <li class="wow rollIn animated" data-wow-duration="3s"><a href="#contact-map" class="contact-map" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i></a></li>
            </ul>            
          </div>
        </div>
      </div>
       <!-- Tab panes -->            
      <div class="tab-content">
        <div class="tab-pane contact-info" id="contact-info">
          <div class="container">
            <div class="row">
            <div class="col-sm-12 text-center">            
              <span class="sub-head">we are here</span>
              <div class="title">
                <h2>get in touch</h2>
              </div>
            </div>
          </div>
            <div class="row text-center">
              <div class="col-sm-4">
                <div class="info-holder">
                  <i class="fa fa-taxi"></i>
                   <br>
                   <p>
                     14/4 <br>
                     Sleman <br>
                     Jogja, Indonesia 
                   </p>
                </div>                
              </div>
              <div class="col-sm-4">
                <div class="info-holder">
                  <i class="fa fa-mobile"></i>
                  <br>
                  <p>+1778-885-0000
                  <br>
                  +1778-885-1111
                  <br>
                  +6698 55514 68
                  </p>
                </div>                
              </div>
              <div class="col-sm-4">
                <div class="info-holder">
                  <i class="fa fa-reply"></i>
                  <br>
                  <p>
                    sinawu@gmail.com
                    <br>
                    michael.abadi@ti.ukdw.ac.id
                  </p>
                </div>                
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane active contact-info" id="contact-form">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 text-center">            
                <span class="sub-head">we are here</span>
                <div class="title">
                  <h2>get in touch</h2>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <form class="form-horizontal" role="form">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <div class="span-form">
                        <input class="form-control" type="text" placeholder="First name" required="required">
                      </div>
                      <div class="span-form">
                        <input type="text" class="form-control" placeholder="Last name" required="required">
                      </div>
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control" placeholder="Email address" required="required">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Subject" required="required">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <textarea name="" class="form-control btn-block" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-block" value="send message">
                    </div>
                  </div>
				  <div class="col-sm-12">
					<p class="contact-success">Your Message has been Successfully Sent!</p>
					<p class="contact-error">Error! Something went wrong!</p>
				  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane contact-info" id="contact-map">
          <div class="container">
            <div class="row">
              <div class="col-sm-12 text-center">            
                <span class="sub-head">we are here</span>
                <div class="title">
                  <h2>get in touch</h2>
                </div>
              </div>
            </div>
          </div>
          <div id="map">
            
          </div>
        </div>
      </div>  
    </section>
    <!-- contact end -->

    <!-- footer start -->
    <footer id="footer">
      <div class="trans-bg">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 text-center">
              <div class="social-icon">
               <a href="" class="wow rollIn animated" data-wow-delay=".1s"><i class="fa fa-facebook"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".2s"><i class="fa fa-twitter"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".3s"><i class="fa fa-google-plus"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".4s"><i class="fa fa-pinterest"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".5s"><i class="fa fa-rss"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".6s"><i class="fa fa-linkedin"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".7s"><i class="fa fa-vimeo-square"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".8s"><i class="fa fa-youtube-square"></i></a>
               <a href="" class="wow rollIn animated" data-wow-delay=".9s"><i class="fa fa-dribbble"></i></a>
              </div>
              <div class="copyright">
                <p>&copy; <span>SINAWU</span>2015, all right reserved</p>
                <p>Design by <span>UKDW-3</span></p>
              </div>
            </div>
          </div>
        </div>
        <div id="go-to-top">
            <a href="#banner"></a>
        </div>
        <!--/.go-to-top end-->
      </div> <!-- /.trans-bg -->
    </footer>
    <!-- footer end -->   
     


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <!-- <script src="https://code.jquery.com/jquery.js"></script> -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/smoothscroll.js"></script>
	  <script src="assets/js/jquery.scrollTo.min.js"></script>
	  <script src="assets/js/jquery.localScroll.min.js"></script>
    <script src="assets/js/jquery.nav.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.magnific-popup.js"></script>
    <script src="assets/js/jquery.parallax.js"></script>
    <script src="assets/js/jquery.flexslider-min.js"></script>
    <script src="assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="assets/js/matchMedia.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/easing.js"></script>
    
    <script>
    	$(document).ready(function() {
		// "use strict";
        $('#banner').parallax("50%", 0.5, true);
    		$('#feature').parallax("50%", 0.5, true);
    		$('#video').parallax("50%", 0.5, true);
    		$('#subscription').parallax("50%", 0.5, true);
    		$('#review').parallax("50%", 0.5, true);
    		$('#download').parallax("50%", 0.5, true);
        $('#footer').parallax("50%", 0.5, true);
    	});

    </script>

    <script>
      // --------Google map---------------
		 var map;
		 function initialize() {  
		  // Create an array of styles.
		  var styles = [
			{
			  stylers: [
				{ hue: "#0AABE1" },
				{ saturation: 0 }
			  ]
			},
			{
			  featureType: 'water',
			  stylers: [
			   { visibility: "on" },
			   { color: "#9a9efd" },
			   { weight: 2.2 },
			   { gamma: 2.54 }
			  ] 
			},
			{
			  featureType: "road",
			  elementType: "geometry",
			  stylers: [
				{ lightness: 100 },
				{ visibility: "simplified" }
			  ]
			},{
			  featureType: "road",
			  elementType: "labels",
			  stylers: [
				{ visibility: "off" }
			  ]
			}
		  ];

		  // Create a new StyledMapType object, passing it the array of styles,
		  // as well as the name to be displayed on the map type control.
		  var styledMap = new google.maps.StyledMapType(styles,
			{name: "Styled Map"});

		  // Create a map object, and include the MapTypeId to add
		  // to the map type control.
		  var losAngeles = new google.maps.LatLng(33.8030716,-118.0725641);
		  var mapOptions = {
			zoom: 13,
			center: losAngeles,
			mapTypeControlOptions: {
			  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
			},
			scrollwheel: false,
		  };
		  var map = new google.maps.Map(document.getElementById('map'),
			mapOptions);
		  

		  //Associate the styled map with the MapTypeId and set it to display.
		  map.mapTypes.set('map_style', styledMap);
		  map.setMapTypeId('map_style');

		  // To add the marker to the map, use the 'map' property
		  var image = 'assets/images/map_icon_3.png';
		  var marker = new google.maps.Marker({
			  position: losAngeles,
			  map: map,
			  title:"GRAPE App Store!",
			  icon: image
		  });

		 }
		
    </script>

    <script type="text/javascript">
     // ref: http://stackoverflow.com/a/1293163/2343
    // This will parse a delimited string into an array of
    // arrays. The default delimiter is the comma, but this
    // can be overriden in the second argument.
    function CSVToArray( strData, strDelimiter ){
        // Check to see if the delimiter is defined. If not,
        // then default to comma.
        strDelimiter = (strDelimiter || ",");

        // Create a regular expression to parse the CSV values.
        var objPattern = new RegExp(
            (
                // Delimiters.
                "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

                // Quoted fields.
                "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

                // Standard fields.
                "([^\"\\" + strDelimiter + "\\r\\n]*))"
            ),
            "gi"
            );


        // Create an array to hold our data. Give the array
        // a default empty first row.
        var arrData = [[]];

        // Create an array to hold our individual pattern
        // matching groups.
        var arrMatches = null;


        // Keep looping over the regular expression matches
        // until we can no longer find a match.
        while (arrMatches = objPattern.exec( strData )){

            // Get the delimiter that was found.
            var strMatchedDelimiter = arrMatches[ 1 ];

            // Check to see if the given delimiter has a length
            // (is not the start of string) and if it matches
            // field delimiter. If id does not, then we know
            // that this delimiter is a row delimiter.
            if (
                strMatchedDelimiter.length &&
                strMatchedDelimiter !== strDelimiter
                ){

                // Since we have reached a new row of data,
                // add an empty row to our data array.
                arrData.push( [] );

            }

            var strMatchedValue;

            // Now that we have our delimiter out of the way,
            // let's check to see which kind of value we
            // captured (quoted or unquoted).
            if (arrMatches[ 2 ]){

                // We found a quoted value. When we capture
                // this value, unescape any double quotes.
                strMatchedValue = arrMatches[ 2 ].replace(
                    new RegExp( "\"\"", "g" ),
                    "\""
                    );

            } else {

                // We found a non-quoted value.
                strMatchedValue = arrMatches[ 3 ];

            }


            // Now that we have our value string, let's add
            // it to the data array.
            arrData[ arrData.length - 1 ].push( strMatchedValue );
        }

        // Return the parsed data.
        return( arrData );
    }
    </script>

    <script type="text/javascript">      
    var SMPcsv="";
    var SMAcsv="";
    var SDcsv="";
    var SMKcsv="";
      $.get('assets/csv/processed-data-putus-sekolah-sd-2008-2011.csv', function(data) {
         SDcsv=data
      }, 'text');
      $.get('assets/csv/processed-data-putus-sekolah-smp-2008-2011.csv', function(data) {
         SMPcsv=data
      }, 'text');
      $.get('assets/csv/processed-data-putus-sekolah-smk-2008-2011.csv', function(data) {
         SMAcsv=data
      }, 'text');
      $.get('assets/csv/processed-data-putus-sekolah-sma-2008-2011.csv', function(data) {
         SMKcsv=data
      }, 'text');
</script>

<script type="text/javascript">
  function rataPutusSekolah(arrayData){
    var tamp=[0,0,0,0]
    var tahun=2008;
    for (var i = 0; i <4; i++) {
      for (var j = arrayData.length - 1; j >= 0; j--) {
        if (arrayData[j][2]==(tahun+i)) {
          tamp[i]+=parseInt(arrayData[j][3]);
        };
      };
    };    
    return( tamp );
  }

</script>

<script type="text/javascript"
          src="https://www.google.com/jsapi?autoload={
            'modules':[{
              'name':'visualization',
              'version':'1',
              'packages':['corechart']
            }]
          }"></script>

<script type="text/javascript"> 
      google.setOnLoadCallback(drawChart);
      var SDarray=CSVToArray(SDcsv);
      var SMParray=CSVToArray(SMPcsv);
      var SMAarray=CSVToArray(SMAcsv);
      var SMkarray=CSVToArray(SMKcsv);
      var arrayTotalPutusSekolah=[
          ['Year', 'SD','SMP','SMA','SMK'],
          ['2008',  0,0,0,0],
          ['2009',  0,0,0,0],
          ['2010',  0,0,0,0],
          ['2011',  0,0,0,0]
           
        ]
      var tamp=rataPutusSekolah(SDarray);
      for (var i = 1; i <5; i++) {
        arrayTotalPutusSekolah[1][i]=tamp[i-1];
      };
      tamp=rataPutusSekolah(SMParray);
      for (var i = 1; i <5; i++) {
        arrayTotalPutusSekolah[2][i]=tamp[i-1];
      };
      tamp=rataPutusSekolah(SMParray);
      for (var i = 1; i <5; i++) {
        arrayTotalPutusSekolah[3][i]=tamp[i-1];
      };
      tamp=rataPutusSekolah(SMAarray);
      for (var i = 1; i <5; i++) {
        arrayTotalPutusSekolah[4][i]=tamp[i-1];
      };

      function drawChart() {
        var data = google.visualization.arrayToDataTable(arrayTotalPutusSekolah);

        var options = {
          title: 'Siswa Putus Sekolah',
          //curveType: 'function',
          legend: { position: 'bottom' },
          vAxis: {title: 'Sekolah'},
          hAxis: {title: 'Tahun'},
          seriesType: 'bars',
          colors: ['#F44336', '#303F9F', '#00BCD4','#607D8B'],
          series: {4: {type: 'line'}}
        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
       
  </body>
</html>