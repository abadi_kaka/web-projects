-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 17, 2015 at 01:46 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sensus_munggur`
--

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE IF NOT EXISTS `agama` (
  `agama_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_agama` varchar(256) NOT NULL,
  PRIMARY KEY (`agama_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`agama_id`, `nama_agama`) VALUES
(1, 'Islam'),
(2, 'Kristen Katolik'),
(3, 'Kristen Protestan'),
(4, 'Hindu'),
(5, 'Budha'),
(6, 'Konghucu'),
(7, 'Aliran Kepercayaan Kepada Tuhan YME'),
(8, 'Aliran Kepercayaan Lain');

-- --------------------------------------------------------

--
-- Table structure for table `aset`
--

CREATE TABLE IF NOT EXISTS `aset` (
  `id` int(11) NOT NULL,
  `kk_id` int(11) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `aset_tanah`
--

CREATE TABLE IF NOT EXISTS `aset_tanah` (
  `tanah_id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(256) NOT NULL,
  PRIMARY KEY (`tanah_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `aset_tanah`
--

INSERT INTO `aset_tanah` (`tanah_id`, `keterangan`) VALUES
(1, 'tidak memiliki tanah'),
(2, 'antara 0,1 - 0,2 Ha'),
(3, 'antara 0,21 - 0,3 Ha'),
(4, 'antara 0,31 - 0,4 Ha'),
(5, 'antara 0,41 - 0,5 Ha'),
(6, 'antara 0,51 - 0,6 Ha'),
(7, 'antara 0,61 - 0,7 Ha'),
(8, 'antara 0,71 - 0,8 Ha'),
(9, 'antara 0,81 - 0,9 Ha'),
(10, 'antara 0,91 - 1,0 Ha'),
(11, 'antara 1,0 - 5,0 Ha'),
(12, 'lebih dari 5,0 Ha');

-- --------------------------------------------------------

--
-- Table structure for table `berobat`
--

CREATE TABLE IF NOT EXISTS `berobat` (
  `obat_id` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_kk`
--

CREATE TABLE IF NOT EXISTS `detail_kk` (
  `no_urut` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(256) NOT NULL,
  `jenis_kelamin` varchar(2) NOT NULL,
  `hubungan` varchar(30) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `umur` int(11) NOT NULL,
  `status_kawin` varchar(30) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `gol_darah` varchar(10) NOT NULL,
  `etnis` varchar(128) NOT NULL,
  `pendidikan` varchar(128) NOT NULL,
  `mata_pencaharian` varchar(128) NOT NULL,
  `orangtua` varchar(50) NOT NULL,
  `no_akta` varchar(20) NOT NULL,
  `akseptor_kb` varchar(50) NOT NULL,
  `cacat_menurut_jenis` varchar(50) NOT NULL,
  `status_milik_rumah` varchar(40) NOT NULL,
  `penghasilan_perbulan` varchar(40) NOT NULL,
  `pengeluaran_perbulan` varchar(40) NOT NULL,
  `lembaga_pemerintahan` varchar(256) NOT NULL,
  `lembaga_kemasyarakatan` varchar(245) NOT NULL,
  `kk` int(11) NOT NULL,
  `pengisi_form` int(11) NOT NULL,
  PRIMARY KEY (`no_urut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hubungan`
--

CREATE TABLE IF NOT EXISTS `hubungan` (
  `hub_id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(256) NOT NULL,
  PRIMARY KEY (`hub_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `hubungan`
--

INSERT INTO `hubungan` (`hub_id`, `keterangan`) VALUES
(1, 'Kepala Keluarga'),
(2, 'Suami'),
(3, 'Istri'),
(4, 'Anak Kandung'),
(5, 'Anak Angkat'),
(6, 'Ayah'),
(7, 'Paman'),
(8, 'Tante'),
(9, 'Kakak'),
(10, 'Adik'),
(11, 'Kakek'),
(12, 'Nenek'),
(13, 'Sepupu'),
(14, 'Keponakan'),
(15, 'Teman');

-- --------------------------------------------------------

--
-- Table structure for table `imunisasi`
--

CREATE TABLE IF NOT EXISTS `imunisasi` (
  `imunisasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`imunisasi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `keluarga`
--

CREATE TABLE IF NOT EXISTS `keluarga` (
  `kk_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kk` varchar(256) NOT NULL,
  `alamat` varchar(256) NOT NULL,
  `rt` varchar(10) NOT NULL,
  `rw` varchar(10) NOT NULL,
  `dusun` varchar(100) NOT NULL,
  `desa` varchar(100) NOT NULL,
  `kecamatan` varchar(256) NOT NULL,
  `kabupaten` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `pengisi` int(11) NOT NULL,
  PRIMARY KEY (`kk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `keluarga`
--

INSERT INTO `keluarga` (`kk_id`, `nama_kk`, `alamat`, `rt`, `rw`, `dusun`, `desa`, `kecamatan`, `kabupaten`, `provinsi`, `tanggal`, `pengisi`) VALUES
(1, 'Yoas Hernanda', 'Munggur', '001', '006', 'Munggur', 'Watusigar', 'Ngawen', 'Gunung Kidul', 'Yogyakarta', '2015-06-10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kepemilikan_lahan`
--

CREATE TABLE IF NOT EXISTS `kepemilikan_lahan` (
  `lahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(256) NOT NULL,
  PRIMARY KEY (`lahan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `kepemilikan_lahan`
--

INSERT INTO `kepemilikan_lahan` (`lahan_id`, `keterangan`) VALUES
(16, 'tidak memiliki'),
(17, 'kurang dari 0,5 Ha'),
(18, 'antara 0,5 - 1,0 Ha'),
(19, 'lebih dari 1,0 Ha');

-- --------------------------------------------------------

--
-- Table structure for table `komoditas`
--

CREATE TABLE IF NOT EXISTS `komoditas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kk_id` int(11) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `produksi` int(11) NOT NULL,
  `pemasaran` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kualitas_ibu_hamil`
--

CREATE TABLE IF NOT EXISTS `kualitas_ibu_hamil` (
  `hamil_id` int(11) NOT NULL AUTO_INCREMENT,
  `keteragan` varchar(256) NOT NULL,
  PRIMARY KEY (`hamil_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `kualitas_ibu_hamil`
--

INSERT INTO `kualitas_ibu_hamil` (`hamil_id`, `keteragan`) VALUES
(1, 'Ibu hamil periksa di posyandu'),
(2, 'Ibu hamil periksa di rumah sakit'),
(3, 'Ibu hamil periksa di dokter praktek'),
(4, 'Ibu hamil periksa di bidan praktek'),
(5, 'Ibu hamil periksa di dukun terlatih'),
(6, 'Ibu hamil tidak periksa kesehatan'),
(7, 'Ibu hamil yang meninggal'),
(8, 'Ibu hamil nifas sakit'),
(9, 'kematian ibu nifas'),
(10, 'ibu nifas sehat'),
(11, 'kematian saat ibu  melahirkan'),
(12, 'tidak ada');

-- --------------------------------------------------------

--
-- Table structure for table `master_komoditas`
--

CREATE TABLE IF NOT EXISTS `master_komoditas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `master_komoditas`
--

INSERT INTO `master_komoditas` (`id`, `jenis`) VALUES
(1, 'jagung'),
(2, 'padi'),
(3, 'kedelai'),
(4, 'kacang_tanah'),
(5, 'singkong'),
(6, 'tembakau'),
(7, 'pisang');

-- --------------------------------------------------------

--
-- Table structure for table `pemasaran`
--

CREATE TABLE IF NOT EXISTS `pemasaran` (
  `pemasaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(256) NOT NULL,
  PRIMARY KEY (`pemasaran_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `pemasaran`
--

INSERT INTO `pemasaran` (`pemasaran_id`, `keterangan`) VALUES
(1, 'dijual ke konsumen'),
(2, 'dijual ke pasar'),
(3, 'dijual melalui KUD'),
(4, 'dijual melalui tengkulak'),
(5, 'dijual melalui pengecer'),
(6, 'dijual ke lumbung pangan desa'),
(7, 'tidak dijual');

-- --------------------------------------------------------

--
-- Table structure for table `penghasilan`
--

CREATE TABLE IF NOT EXISTS `penghasilan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `penghasilan`
--

INSERT INTO `penghasilan` (`id`, `keterangan`) VALUES
(5, 'di Bawah Rp.500.000,-'),
(6, 'Rp.1.000.000,- s/d Rp.2.000.000,-'),
(7, 'Rp.2.000.000,- s/d Rp.3.000.000,-'),
(8, 'lebih dari Rp.3.000.000,-');

-- --------------------------------------------------------

--
-- Table structure for table `perumahan`
--

CREATE TABLE IF NOT EXISTS `perumahan` (
  `id` int(11) NOT NULL,
  `kk_id` int(11) NOT NULL,
  `dinding` varchar(50) NOT NULL,
  `lantai` varchar(50) NOT NULL,
  `atap` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ternak`
--

CREATE TABLE IF NOT EXISTS `ternak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kk_id` int(11) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pengisi` varchar(256) NOT NULL,
  `pekerjaan` varchar(256) NOT NULL,
  `jabatan` varchar(256) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` varchar(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `nama_pengisi`, `pekerjaan`, `jabatan`, `username`, `password`, `role`) VALUES
(1, 'Yerimias Christian', 'mahasiswa', '-', 'jerry', 'jerry', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wc`
--

CREATE TABLE IF NOT EXISTS `wc` (
  `wc_id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(50) NOT NULL,
  PRIMARY KEY (`wc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
