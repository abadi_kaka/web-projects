<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('my_model');		
	}

	public function index()
	{
		if ($this->session->userdata('user_input')['username'] != "") {
			redirect('welcome/inputdata');
		}
		$this->load->view('home');
	}

	public function inputdata()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		$data['hubungan'] = $this->my_model->get_hubungan();
		$data['agama'] = $this->my_model->get_agama();
		$data['penghasilan'] = $this->my_model->get_penghasilan();
		$data['pengeluaran'] = $data['penghasilan'];
		$data['lahan'] = $this->my_model->get_lahan();
		$data['pemasaran'] = $this->my_model->get_pemasaran();
		$data['komoditas'] = $this->my_model->get_master_komoditas();
		$this->load->view('inputdata', $data);	
	}

	public function process_login()
	{
		$this->load->model('My_model');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('userpass', 'Password', 'required|trim|xss_clean|max_length[50]');

		if ($this->form_validation->run() == TRUE) {
			// if validation run
			$array = array('username' => $this->input->post('username'), 'password' => $this->input->post('userpass'));
			if ($query = $this->my_model->login_validation($array)) {
				//jika berhasil, redirect juga
				
				$this->session->set_userdata('user_input', $query);
				$this->session->set_flashdata('message', 'berhasil');
				redirect('welcome/inputdata');
			} else {
				$this->session->set_flashdata('message', '* Akun anda tidak dapat ditemukan');
			}
		} else {
			$this->session->set_flashdata('message', validation_errors());
		}
		redirect('welcome');
	}

	public function logout()
	{
		$this->session->unset_userdata('user_input');
		redirect('welcome');
	}

	public function insert_kk()
	{
		$array = array(
				'nama_kk' => $this->input->post('nama_kk'),
				'alamat' => $this->input->post('alamat_kk'),
				'rt' => $this->input->post('rt_kk'),
				'rw' => $this->input->post('rw_kk'),
				'dusun' => $this->input->post('dusun'),
				'desa' => $this->input->post('kelurahan'),
				'kecamatan' => $this->input->post('kecamatan'),
				'kabupaten' => $this->input->post('kabupaten'),
				'provinsi' => $this->input->post('provinsi'),
				'tanggal' => date('Y-m-d'),
				'pengisi' => $this->session->userdata('user_input')['user_id']
			);

		$query = $this->my_model->insert_kk($array);
		if ($query) {
			redirect('welcome/inputdata');
		}else{

		}
	}

	public function inputddk()
	{
		# code...
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */