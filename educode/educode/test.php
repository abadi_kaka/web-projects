<!DOCTYPE html>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script src="./img/twitterlib.js"></script>
    <title>DATA POKOK PENDIDIKAN - DINAS PENDIDIKAN PROVINSI DKI JAKARTA</title>
    <link rel="stylesheet" href="./img/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="./img/nivo-slider.css" type="text/css" media="screen">  
    <link rel="stylesheet" href="./img/switcher.css" type="text/css" media="screen"> 
  <style type='text/css'>
  #peta {
  width: 1080px;
  height: 600px;

} </style>
	<style type="text/css"></style>
    
    <!--[if IE 7]>
    <link rel="stylesheet" media="screen" href="css/ie7.css"/>
  	<![endif]-->
   
	<script src="./img/user_timeline.json" id="twitterlib1382715557156"></script>
	
<script type="text/javascript" src="jquery-1.6.2.min.js"></script>
<script type="text/javascript">
$(function () {

    var colors = Highcharts.getOptions().colors,
        categories = ['TK', 'SD', 'SMP', 'SMA', 'SMK', 'PLB', 'RA', 'MI', 'MTS', 'MA'],
        name = 'Jenjang Pendidikan',
        data = [{
                y: 2129,
                color: colors[0],
                drilldown: {
                    name: 'Taman Kanak-kanak (TK)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [219, 266, 464, 474, 691, 10],
	                color: colors[0],
                }
            }, {
                y: 2957,
                color: colors[1],
                drilldown: {
                    name: 'Sekolah Dasar (SD)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [351, 453, 690, 650, 794, 17],
                    color: colors[1]
                }
            }, {
                y: 1340,
                color: colors[2],
                drilldown: {
                    name: 'Sekolah Menengah Pertama (SMP)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [142, 232, 336, 282, 335, 9],
                    color: colors[2]
                }
            }, {
                y: 710,
                color: colors[3],
                drilldown: {
                    name: 'Sekolah Menengah Atas (SMA)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [81, 120, 173, 154, 176, 3],
                    color: colors[3]
                }
            }, {
                y: 609,
                color: colors[4],
                drilldown: {
                    name: 'Sekolah Menengah Kejuruan (SMK)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [68, 78, 123, 140, 199, 1],
                    color: colors[4]
                }
            }, {
                y: 86,
                color: colors[4],
                drilldown: {
                    name: 'Pendidikan Luar Biasa (PLB)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [7, 5, 17, 28, 22, 0],
                    color: colors[4]
                }
            }, {
                y: 1021,
                color: colors[0],
                drilldown: {
                    name: 'Raudhatul Athfal (RA)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [1, 3, , 1, , ],
	                color: colors[0],
                }
            }, {
                y: 469,
                color: colors[1],
                drilldown: {
                    name: 'Madrasah Ibtidaiyah (MI)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [8, 23, 81, 108, 70, ],
                    color: colors[1]
                }
            }, {
                y: 245,
                color: colors[2],
                drilldown: {
                    name: 'Madrasah Tsanawiyah (MTS)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [6, 23, 19, 44, 53, ],
                    color: colors[2]
                }
            }, {
                y: 92,
                color: colors[3],
                drilldown: {
                    name: 'Madrasah Aliyah (MA)',
                    categories: ['Jakarta Pusat', 'Jakarta Utara', 'Jakarta Barat', 'Jakarta Selatan', 'Jakarta Timur', 'Kepulauan Seribu'],
                    data: [6, 11, 9, 16, 27, 1],
                    color: colors[3]
                }
            }];

    function setChart(name, categories, data, color) {
chart.xAxis[0].setCategories(categories, false);
chart.series[0].remove(false);
chart.addSeries({
name: name,
data: data,
color: color || 'white'
}, false);
chart.redraw();
    }

    var chart = $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'GRAFIK DATA SEKOLAH DI LINGKUNGAN DINAS PENDIDIKAN PROVINSI DKI JAKARTA TAHUN AJARAN 2015'
        },
        subtitle: {
            text: 'Klik bar untuk melihat grafik sekolah perjenjang pada tiap kotamadya/kabupaten kota'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: 'Total jumlah sekolah'
            }
        },
        plotOptions: {
            column: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            var drilldown = this.drilldown;
                            if (drilldown) { // drill down
                                setChart(drilldown.name, drilldown.categories, drilldown.data, drilldown.color);
                            } else { // restore
                                setChart(name, categories, data);
                            }
                        }
                    }
                },
                dataLabels: {
                    enabled: true,
                    color: colors[0],
                    style: {
                        fontWeight: 'bold'
                    },
                    formatter: function() {
                        //return this.y +'%';
						return this.y;
                    }
                }
            }
        },
        tooltip: {
            formatter: function() {
                var point = this.point,
                    s = '<b>' + this.x +':'+ this.y +' sekolah</b><br/>';
                if (point.drilldown) {
                    s += 'Klik untuk melihat grafik '+ point.category +' per kotamadya / kabupaten kota';
                } else {
                    s += 'Klik untuk kembali ke grafik provinsi';
                }
                return s;
            }
        },
        series: [{
            name: name,
            data: data,
            color: 'white'
        }],
        exporting: {
            enabled: false
        }
    })
    .highcharts(); // return chart
});
</script>
</head>

<body>	
	<div id="wrapper">  
	<table width="1080" border="0" cellpadding="0" cellspacing="0">
	<tr height="170"><td align="center"><img src="img/header.jpg" title="disdikdki.net" align="center"></td></tr>
	<tr height="22"><td><ul id="nt-title"><li>Untuk permintaan user id dan login password datadik silahkan hubungi bapak Andi 081298876208 atau bp David 085719111613</li><li>Penempatan tempat tugas nantinya bagi para PNS termasuk Guru bantu yang diangkat berdasarkan data tempat tinggal yang ada di datadikdki.net</li><li>Datadikdki.net satu satunya data yang digunakan Dinas Pendidikan Provinsi untuk pengambilan kebijakan jadi pastikan data bapak ibu tercantum</li><li>Sesuai kebijakan akan ada pengangkatan guru bantu untuk proses pengangkatan CPNS secara bertahap, pastikan data alamat tempat tinggal lengkap termasuk nomor telepon sudah terisi </li>	</ul>
    <script src="js/jquery.newsTicker.js"></script>
    <script>
    var nt_title = $('#nt-title').newsTicker({
        row_height: 20,
        max_rows: 1,
        duration: 7000,
        pauseOnHover: 0
    });
	</script></td></tr>
	</table>
    
    	<!-- HEADER START -->
		<div id="header">
        	<div class="inner">
            	
                        	<div class="nav">
                    <ul> 
                        <li class="current"><a href="index.php">Beranda</a></li>
                        <li><a href="#">Sekolah</a>
                        	<ul style="visibility: hidden;">
                                <li><a href="?mn=sekolah&jjg=tk">TK</a></li>
                                <li><a href="?mn=sekolah&jjg=sd">SD</a></li>
                                <li><a href="?mn=sekolah&jjg=smp">SMP</a></li>
                                <li><a href="?mn=sekolah&jjg=sma">SMA</a></li>
                                <li><a href="?mn=sekolah&jjg=smk">SMK</a></li>
                                <li><a href="?mn=sekolah&jjg=plb">PLB</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Madrasah</a>
                        	<ul style="visibility: hidden;">
                                <li><a href="?mn=sekolah&jjg=ra">RA</a></li>
                                <li><a href="?mn=sekolah&jjg=mi">MI</a></li>
                                <li><a href="?mn=sekolah&jjg=mts">MTS</a></li>
                                <li><a href="?mn=sekolah&jjg=ma">MA</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Guru</a>
                        	<ul style="visibility: hidden;">
                                <li><a href="?mn=guru&jjg=tk">TK</a></li>
                                <li><a href="?mn=guru&jjg=sd">SD</a></li>
                                <li><a href="?mn=guru&jjg=smp">SMP</a></li>
                                <li><a href="?mn=guru&jjg=sma">SMA</a></li>
                                <li><a href="?mn=guru&jjg=smk">SMK</a></li>
                                <li><a href="?mn=guru&jjg=plb">PLB</a></li>
                                <li><a href="?mn=guru&jjg=ra">RA</a></li>
                                <li><a href="?mn=guru&jjg=mi">MI</a></li>
                                <li><a href="?mn=guru&jjg=mts">MTS</a></li>
                                <li><a href="?mn=guru&jjg=ma">MA</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Rekapitulasi</a>
                        	<ul>                            	
                                <li><a href="?mn=rangkuman&data=sekolah">Sekolah</a></li>
                                <li><a href="?mn=rangkuman&data=madrasah">Madrasah</a></li>
                                <li><a href="?mn=rangkuman&data=guru">Guru</a></li>
                                <li><a href="?mn=rangkuman&data=siswa">Siswa</a></li>
                            </ul>
                        </li>
                        <li><a href="?mn=grafik">GRAFIK</a></li>
                        <li><a href="http://opr.datadikdki.net/login.php" target="_blank">AREA OPERATOR</a></li>
                    </ul>  
            	</div><!-- #nav end -->
                <div class="clear"></div> 
            </div><!-- .inner end -->  
            
        </div><!-- #header end -->
        <!-- HEADER END -->      	        
        <!-- MAIN CONTENT START -->
        <div id="main">
			<form name="fform" action="" method="post" ENCTYPE="MULTIPART/FORM-DATA">
    		<div id="inner">
			<br />
				<script src="highcharts.js"></script>
				<div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
				            </div><!-- .inner End --> 
			</form>
        </div><!-- #main End --> 
        <!-- MAIN CONTENT END --> 
        
		<!-- FOOTER START --> 
                <div class="divider line"></div> 
        <div id="footer"> 
        	<div class="inner">       
            	                 
                <div class="one_fourth">
					<img src="img/logo.jpg" title="disdikdki.net">
                                                          
                </div>
                
                <div class="one_fifth">
                    <h5>Link Terkait</h5> 
					<a href="http://www.kemdikbud.go.id" target="_blank">Kementerian Pendidikan dan Kebudayaan RI</a><br /><a href="http://disdik.jakarta.go.id" target="_blank">Dinas Pendidikan Provinsi DKI Jakarta</a><br /><a href="http://www.simdik.info" target="_blank">Sistem Informasi Manajemen Pendidikan (SIMDIK)</a><br /><a href="http://pengaduan.disdikdki.info" target="_blank">Pelayanan dan Pengaduan Masyarakat (KJP & BOP)</a><br /><a href="http://www.smkdki.net" target="_blank">Portal SMK Provinsi DKI Jakarta</a><br />				<!--<a href="http://www.kemdikbud.go.id/" target="_blank">Kementerian Pendidikan dan Kebudayaan RI</a><br />
				<a href="http://disdik.jakarta.go.id/" target="_blank">Dinas Pendidikan Provinsi DKI Jakarta
				</a><br />
				<a href="http://www.simdik.info/" target="_blank">Sistem Informasi Manajemen Pendidikan (SIMDIK)</a><br />
				<a href="http://pengaduan.disdikdki.info/" target="_blank">Pelayanan dan Pengaduan Masyarakat (KJP & BOP)</a><br />
				<a href="http://www.smkdki.net/" target="_blank">Portal SMK DKI Jakarta</a><br />//-->
                 </div>  
                             
                <div class="one_fifth"> 
                    <a name="hubungi"></a>
					<h5>Hubungi Kami</h5> 
                    <p> 
                    UPT Pusat Data dan Sistem Informasi Pendidikan (PDSIP)<br>Dinas Pendidikan Provinsi DKI Jakarta<br>Jl. Gatot Subroto Kav. 40-41<br>Jakarta Selatan<br>
                    </p> 
                    <p> 
                    Telp: 021 5272445<br> 
                    HP: 0857 1911 16 13, 0812 98876208, 0812 1879 0899, 0816 1920 255, 0822 1373 2049, 0812 1249 1349<br> 
                    E-Mail: disdik@jakarta.go.id
                    </p> 
                </div>                
       
            </div><!-- .inner End -->  
          
            <div class="footer_small">             
            	<div class="copyright">
            		© 2013 <a href="http://www.datadikdki.net/">datadikdki</a> - All rights reserved.
            	</div>
                
                <div class="social"> 
            		<a href="#"><img src="./img/twitter.png" alt="" style="opacity: 0.5;"></a>
                    <a href="https://www.facebook.com/uptpdsip" target="_blank"><img src="./img/facebook.png" alt="" style="opacity: 0.5;">
                </div> 
            </div> <!-- #footer_small End -->
            
        </div><!-- #footer End -->   
        <!-- FOOTER END -->        
    </div><!-- #holder End --> 
    <!-- PAGE END --> 
    
    
    <!-- jQuery Scripts -->
    <script type="text/javascript" src="custom.js"></script>  
    <script type="text/javascript" src="jquery.nivo.slider.pack.js"></script> 
    <script type="text/javascript" src="jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="jquery-ui-personalized-1.5.2.packed.js"></script>   
    <script type="text/javascript" src="styleswitcher.js"></script>
    <script src="./img/twitter.min.js" type="text/javascript"></script>  
    <script type="text/javascript" src="jquery.quicksand.min.js"></script> 
    <script type="text/javascript" src="jquery.easing.js"></script>
    
</body></html>